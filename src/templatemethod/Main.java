package templatemethod;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Dice game = new Dice();
		System.out.println("How many players?");
		Scanner sc = new Scanner(System.in);
		int playersCount = sc.nextInt() -1;
		game.playOneGame(playersCount);

	}

}
