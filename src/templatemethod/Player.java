package templatemethod;

import java.util.Scanner;

public class Player {

	String name = null;
	Scanner sc = new Scanner(System.in);
	int score = 0;
	int finalScore = 0;
	int player = 0;

	public int getFinalScore() {
		return finalScore;
	}

	public void setFinalScore(int score) {
		this.finalScore += score;
	}

	public Player(int player) {
		System.out.println("What is the name of the player?");
		this.name = sc.next();
		this.player = player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getPlayer() {
		return player;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}