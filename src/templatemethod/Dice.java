package templatemethod;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Dice extends Game {

	Random randGenerator = new Random();
	String winner = null;
	int dice;
	int dice1;
	Scanner sc = new Scanner(System.in);
	ArrayList<Player> players = new ArrayList<Player>();

	@Override
	void initializeGame(int playersCount) {
		for (int i = 0; i <= playersCount; i++) {
			players.add(new Player(i));
		}
	}

	@Override
	void makePlay(int player) {
		for (Player player1 : players) {
			dice = randGenerator.nextInt(6) + 1;
			dice1 = randGenerator.nextInt(6) + 1;
			System.out.println("Player " + player1.getName() + " has " + player1.getFinalScore());
			System.out.println("Player " + player1.getName() + " throws " + dice + " " + dice1);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int total = dice + dice1;
			player1.setFinalScore(total);
		}
	}

	@Override
	boolean endOfGame() {
		for (Player player1 : players) {
			if (player1.getFinalScore() >= 100) {
				winner = player1.getName();
				return true;
			}
		}
		return false;
	}

	@Override
	void printWinner() {
		System.out.println("Player " + winner + " has won the game");
	}

}
